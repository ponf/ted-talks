//
// Created by panfilov on 17/04/15.
// Copyright (c) 2015 Filipp Panfilov. All rights reserved.
//

#import "MainModel.h"
#import "NetworkingService.h"


@implementation MainModel {
    NetworkingService *_networkingService;
}

- (instancetype)init {
    if (self = [super init]){
        _networkingService = [NetworkingService new];
    }
    return self;
}

- (void)loadTEDTalksCompleted:(void (^)(NSArray *))completed {
    [_networkingService loadTEDTalksCompleted:completed];
}

@end