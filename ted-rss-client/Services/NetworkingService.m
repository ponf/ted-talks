//
// Created by panfilov on 17/04/15.
// Copyright (c) 2015 Filipp Panfilov. All rights reserved.
//

#import "NetworkingService.h"
#import "AFHTTPRequestOperationManager.h"
#import "XMLDictionary.h"
#import "TEDItemDTO.h"

NSString * const kTedTalksUrl = @"http://www.ted.com/themes/rss/id/6";

@implementation NetworkingService {
}

#pragma mark -- Public

- (void)loadTEDTalksCompleted:(void (^)(NSArray *))completed {
    void (^safeCompleted)(NSArray *) = ^void(NSArray *talks){
        if (completed)
            completed(talks);
    };

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFXMLParserResponseSerializer *responseSerializer = [AFXMLParserResponseSerializer new];
    responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/rss+xml"]];
    manager.responseSerializer = responseSerializer;
    [manager GET:kTedTalksUrl
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, NSXMLParser *parser) {
             NSArray *talks = [self prepareTalksResponseFromXMLParser:parser];
             safeCompleted(talks);
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             safeCompleted(nil);
         }];
}

#pragma mark -- Private

- (NSArray *)prepareTalksResponseFromXMLParser:(NSXMLParser *)response {
    NSMutableArray *talks = @[].mutableCopy;
    NSDictionary *rssDictionary = [[XMLDictionaryParser sharedInstance] dictionaryWithParser:response];
    NSArray *items = rssDictionary[@"channel"][@"item"];

    for (NSDictionary *item in [items reverseObjectEnumerator]) {
        TEDItemDTO *movieDTO = [TEDItemDTO movieWithTitle:item[@"title"]
                                                  preview:item[@"itunes:image"][@"_url"]
                                              publishDate:item[@"pubDate"]
                                                 movieUrl:item[@"enclosure"][@"_url"]
                                                 duration:item[@"itunes:duration"]
                                              description:item[@"description"]
                                                     link:item[@"link"]];
        [talks addObject:movieDTO];
    }

    return talks;
}

@end