//
// Created by panfilov on 17/04/15.
// Copyright (c) 2015 Filipp Panfilov. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NetworkingService : NSObject

- (void)loadTEDTalksCompleted:(void (^)(NSArray *))completed;

@end