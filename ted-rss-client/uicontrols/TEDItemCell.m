//
//  TEDItemCell.m
//  ted-rss-client
//
//  Created by panfilov on 17/04/15.
//  Copyright (c) 2015 Filipp Panfilov. All rights reserved.
//

#import "TEDItemCell.h"

@implementation TEDItemCell

- (void)prepareForReuse {
    [super prepareForReuse];
    _previewImage.image = [UIImage new];
}

@end
