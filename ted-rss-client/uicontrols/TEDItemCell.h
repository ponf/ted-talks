//
//  TEDItemCell.h
//  ted-rss-client
//
//  Created by panfilov on 17/04/15.
//  Copyright (c) 2015 Filipp Panfilov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TEDItemCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *title;
@property (nonatomic, strong) IBOutlet UILabel *duration;
@property (nonatomic, strong) IBOutlet UILabel *date;
@property (nonatomic, strong) IBOutlet UIImageView *previewImage;

@end
