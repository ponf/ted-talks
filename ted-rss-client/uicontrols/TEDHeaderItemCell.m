//
//  TEDHeaderItemCell.m
//  ted-rss-client
//
//  Created by panfilov on 18/04/15.
//  Copyright (c) 2015 Filipp Panfilov. All rights reserved.
//

#import "TEDHeaderItemCell.h"

@implementation TEDHeaderItemCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    // Configure the view for the selected state
}

- (IBAction)playButtonPressed:(id)sender {
    [_delegate headerItemCellPlayButtonPressed:self];
}


@end
