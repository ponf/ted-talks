//
//  TEDHeaderItemCell.h
//  ted-rss-client
//
//  Created by panfilov on 18/04/15.
//  Copyright (c) 2015 Filipp Panfilov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TEDHeaderItemCellDelegate;

@interface TEDHeaderItemCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *title;
@property (nonatomic, strong) IBOutlet UIImageView *previewImage;
@property (nonatomic, strong) IBOutlet UILabel *videoDescription;
@property (nonatomic, weak) id <TEDHeaderItemCellDelegate> delegate;

@end

@protocol TEDHeaderItemCellDelegate <NSObject>

- (void)headerItemCellPlayButtonPressed:(TEDHeaderItemCell *)cell;

@end