//
//  VideoControllersFactory.h
//  ted-rss-client
//
//  Created by panfilov on 18/04/15.
//  Copyright (c) 2015 Filipp Panfilov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TEDItemDTO.h"
#import <UIKit/UIKit.h>

@import MediaPlayer;

@interface VideoControllersFactory : NSObject

+ (UIViewController *)videoViewControllerForTEDItemDTO:(TEDItemDTO *)dto;

@end
