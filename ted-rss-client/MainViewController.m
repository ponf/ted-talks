//
//  MainViewController.m
//  ted-rss-client
//
//  Created by panfilov on 17/04/15.
//  Copyright (c) 2015 Filipp Panfilov. All rights reserved.
//

#import "MainViewController.h"
#import "MainModel.h"
#import "TEDItemCell.h"
#import "TEDItemDTO.h"
#import "UIImageView+AFNetworking.h"
#import "TEDHeaderItemCell.h"
#import "VideoControllersFactory.h"

NSString * const kTEDItemCellIdentifier = @"kTEDItemCellIdentifier";
NSString * const kTEDHeaderCellIdentifier = @"kTEDHeaderCellIdentifier";

@interface MainViewController () <UITableViewDelegate, UITableViewDataSource, TEDHeaderItemCellDelegate>

@end

@implementation MainViewController {
    MainModel *_model;
    NSArray *_tedItems;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"TED Talks";
    _model = [MainModel new];
    _tedItems = @[];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    __weak typeof(self) wself = self;
    [_model loadTEDTalksCompleted:^(NSArray *array) {
        typeof(self) sself = wself;
        if (sself) {
            sself->_tedItems = [array copy];
            [sself.tableView reloadData];
        }
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath row] > ([_tedItems count] - 1))
        return nil;
    TEDItemDTO *dto = _tedItems[(NSUInteger)[indexPath row]];
    
    if ([indexPath row] == 0){
        TEDHeaderItemCell *headerCell = [tableView dequeueReusableCellWithIdentifier:kTEDHeaderCellIdentifier];
        if (!headerCell) {
            headerCell = [[TEDHeaderItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kTEDHeaderCellIdentifier];
        }
        headerCell.title.text = dto.title;
        [headerCell.previewImage setImageWithURL:[[NSURL alloc] initWithString:dto.preview]];
        headerCell.videoDescription.text = dto.movieDescription;
        headerCell.delegate = self;
        return headerCell;
    }
    
    TEDItemCell *cell = [tableView dequeueReusableCellWithIdentifier:kTEDItemCellIdentifier];
    if (!cell) {
        cell = [[TEDItemCell alloc] initWithStyle:UITableViewCellStyleDefault
                                  reuseIdentifier:kTEDItemCellIdentifier];
    }

    cell.title.text = dto.title;
    [cell.previewImage setImageWithURL:[[NSURL alloc] initWithString:dto.preview]];
    cell.duration.text = dto.duration;
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_tedItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [indexPath row] == 0 ? 310 : 150;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath row] == 0)
        return;
    
    [self presentViewController:[VideoControllersFactory videoViewControllerForTEDItemDTO:_tedItems[(NSUInteger) [indexPath row]]]
                       animated:YES
                     completion:nil];
}

#pragma mark -- TEDHeaderItemCellDelegate

- (void)headerItemCellPlayButtonPressed:(TEDHeaderItemCell *)cell {
    TEDItemDTO *dto = [_tedItems firstObject];
    [self presentViewController:[VideoControllersFactory videoViewControllerForTEDItemDTO:dto] animated:YES completion:nil];
}

@end
