//
//  VideoControllersFactory.m
//  ted-rss-client
//
//  Created by panfilov on 18/04/15.
//  Copyright (c) 2015 Filipp Panfilov. All rights reserved.
//

#import "VideoControllersFactory.h"

@implementation VideoControllersFactory

#pragma mark -- Public

+ (UIViewController *)videoViewControllerForTEDItemDTO:(TEDItemDTO *)dto {
    NSURL *videoURL = [NSURL URLWithString:dto.movieUrl];
    MPMoviePlayerViewController *viewController = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
    return viewController;
}

@end
