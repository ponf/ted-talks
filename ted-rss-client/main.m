//
//  main.m
//  ted-rss-client
//
//  Created by panfilov on 17/04/15.
//  Copyright (c) 2015 Filipp Panfilov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
