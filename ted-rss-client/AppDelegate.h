//
//  AppDelegate.h
//  ted-rss-client
//
//  Created by panfilov on 17/04/15.
//  Copyright (c) 2015 Filipp Panfilov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

