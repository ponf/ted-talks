//
// Created by panfilov on 17/04/15.
// Copyright (c) 2015 Filipp Panfilov. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TEDItemDTO : NSObject

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *preview;
@property (nonatomic, readonly) NSString *publishDate;
@property (nonatomic, readonly) NSString *movieUrl;
@property (nonatomic, readonly) NSString *duration;
@property (nonatomic, readonly) NSString *movieDescription;
@property (nonatomic, readonly) NSString *link;

+ (instancetype)movieWithTitle:(NSString *)title
                       preview:(NSString *)preview
                   publishDate:(NSString *)publishDate
                      movieUrl:(NSString *)movieUrl
                      duration:(NSString *)duration
                   description:(NSString *)movieDescription
                          link:(NSString *)link;
@end