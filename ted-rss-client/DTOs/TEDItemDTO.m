//
// Created by panfilov on 17/04/15.
// Copyright (c) 2015 Filipp Panfilov. All rights reserved.
//

#import "TEDItemDTO.h"


@implementation TEDItemDTO

- (instancetype)initWithTitle:(NSString *)title
                      preview:(NSString *)preview
                  publishDate:(NSString *)publishDate
                     movieUrl:(NSString *)movieUrl
                     duration:(NSString *)duration
                  description:(NSString *)movieDescription
                         link:(NSString *)link {
    if (self = [super init]){
        _title = title;
        _preview = preview;
        _publishDate = publishDate;
        _movieUrl = movieUrl;
        _duration = duration;
        _movieDescription = movieDescription;
        _link = link;

    }
    return self;
}

+ (instancetype)movieWithTitle:(NSString *)title
                       preview:(NSString *)preview
                   publishDate:(NSString *)publishDate
                      movieUrl:(NSString *)movieUrl
                      duration:(NSString *)duration
                   description:(NSString *)movieDescription
                          link:(NSString *)link {
    return [[TEDItemDTO alloc] initWithTitle:title
                                      preview:preview
                                  publishDate:publishDate
                                     movieUrl:movieUrl
                                     duration:duration
                                  description:movieDescription
                                         link:link];
}

@end